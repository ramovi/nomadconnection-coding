
/**
 * 테스트 코드를 보고, 아래 두 method 를 작성해주세요.
 */
class Questing {
	static int quest1(double d) {
		// FIXME
		return (int) d / 2; // double형을 int형으로 cast 하여 demotion을 표현하였습니다.
	}

	static boolean quest2(String s) {
		// FIXME
		if (Checker(s) == true) {
			return true;
		} else {
			return false;
		}
	}

	static boolean Checker(String s) {
		// Checker
		Stack stack = new Stack(s.length()); // 문자열의 길이만큼 문자 저장공간을 가진 스텍을 생성합니다.
		for (int loop = 0; loop < s.length(); loop++) { // 문자열을 단일문자로 쪼개어 판별 하기위해 저장공간만큼 반복문을 주었습니다.
			char ch = s.charAt(loop); // 문자열에서 해당 index에 위치한 문자를 받아옵니다.
			switch (ch) { // 판별을 위해 switch~case 문을 사용하였습니다.
			case '(': case '{': case '[': // 열림 괄호일 경우 해당 문자를 stack에 push 합니다.
				stack.push(ch);
				break;
			case ')': case '}': case ']': // 닫힘 괄호일 경우일때 조건이 시작됩니다.
				if (stack.isEmpty()) { // stack이 비어있을경우 false를 return 합니다.
					return false;
				}
				if ((stack.stack[loop - 1] == '(') && (ch != ')')|| // 스텍[최상위] 저장된 값이 '('이고, 현재 문자가 ')'가 아닐경우 false.
					(stack.stack[loop - 1] == '{') && (ch != '}')|| // 스텍[최상위] 저장된 값이 '{'이고, 현재 문자가 '}'가 아닐경우 false.
					(stack.stack[loop - 1] == '[') && (ch != ']')) { // 스텍[최상위] 저장된 값이 '['이고, 현재 문자가 ']'가 아닐경우 false.
					return false;
				}
				stack.pop(); // 이외의 상황일 경우 stack[최상위] 를 pop 합니다.
				break;
			}
		}
		if (stack.isEmpty()) { // 반복이 끝난 후 스텍이 비어있을 경우 true. 스텍이 비어있지 않을 경우 false.
			return true;
		} else {
			return false;
		}
	}
}

class Stack { 
	// Stack 한 화면에 보이기위해 하단에 Stack 클래스를 작성하였습니다.

	char[] stack;
	int top;

	public Stack(int size) {
		//constructor
		stack = new char[size];
		this.top = -1;
	}

	public void push(char data) {
		//push
		stack[++top] = data;
	}

	public char pop() {
		//pop
		return stack[top--];
	}

	public boolean isEmpty() {
		//isEmpty
		if (top == -1) {
			return true;
		} else {
			return false;
		}
	}
}
